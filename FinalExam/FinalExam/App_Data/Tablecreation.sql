IF OBJECT_ID('dbo.Classification','U') IS NOT NULL
	DROP TABLE [dbo].[Classification];
GO

IF OBJECT_ID('dbo.Genre','U') IS NOT NULL
	DROP TABLE [dbo].[Genre];
GO

IF OBJECT_ID('dbo.ArtWork','U') IS NOT NULL
	DROP TABLE [dbo].[ArtWork];
GO

IF OBJECT_ID('dbo.Artist','U') IS NOT NULL
	DROP TABLE [dbo].[Artist];
GO

-- ########### Artist ###########
CREATE TABLE [dbo].[Artist]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (50) NOT NULL,
	[Date] Date NOT NULL,
	[City] NVARCHAR (50) NOT NULL
	CONSTRAINT [PK_dbo.Artist] PRIMARY KEY CLUSTERED ([ID] ASC)
);

-- ########### ArtWork ###########
CREATE TABLE [dbo].[ArtWork]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Title] NVARCHAR (50) NOT NULL,
	[Artist] NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_dbo.ArtWork] PRIMARY KEY CLUSTERED ([ID] ASC)
);

-- ########### Genre ###########
CREATE TABLE [dbo].[Genre]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_dbo.Genre] PRIMARY KEY CLUSTERED ([ID] ASC)
);

-- ########### Classification ###########
CREATE TABLE [dbo].[Classification]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[ArtWork] NVARCHAR (50) NOT NULL,
	[Genre]  NVARCHAR (50) NOT NULL,
	CONSTRAINT [PK_dbo.Classification] PRIMARY KEY CLUSTERED ([ID] ASC)
);