-- ########### Artist ###########
INSERT INTO [dbo].[Artist](Name,Date,City)VALUES('M.C. Escher','1898-06-17','Leeuwarden, Netherlands');
INSERT INTO [dbo].[Artist](Name,Date,City)VALUES('Leonardo Da Vinci','1519-05-02','Vinvi, Italy');
INSERT INTO [dbo].[Artist](Name,Date,City)VALUES('Hatip Mehmed Efendi','1680-11-18','Unknown');
INSERT INTO [dbo].[Artist](Name,Date,City)VALUES('Salvador Dali','1904-05-11','Figueres, Spain');

-- ########### Artwork ###########

INSERT INTO [dbo].[Artwork](Title,Artist)VALUES('Circle Limit III','M.C. Escher');
INSERT INTO [dbo].[Artwork](Title,Artist)VALUES('Twon Tree','M.C. Escher');
INSERT INTO [dbo].[Artwork](Title,Artist)VALUES('Mona Lisa','Leonardo Da Vinci');
INSERT INTO [dbo].[Artwork](Title,Artist)VALUES('The Vitruvian Man','Leonardo Da Vinci');
INSERT INTO [dbo].[Artwork](Title,Artist)VALUES('Ebru','Hatip Mehmed Efendi');
INSERT INTO [dbo].[Artwork](Title,Artist)VALUES('Honey Is Sweeter Than Blood','Salvador Dali');


-- ########### Genre ###########
INSERT INTO [dbo].[Genre](Name)VALUES('Tesselation');
INSERT INTO [dbo].[Genre](Name)VALUES('Surrealism');
INSERT INTO [dbo].[Genre](Name)VALUES('Portrait');
INSERT INTO [dbo].[Genre](Name)VALUES('Renaissance');


-- ########### Classification ###########

INSERT INTO [dbo].[Classification](ArtWork,Genre)VALUES('Circle Limit III', 'Tesselation');
INSERT INTO [dbo].[Classification](ArtWork,Genre)VALUES('Twon Tree', 'Tesselation');
INSERT INTO [dbo].[Classification](ArtWork,Genre)VALUES('Twon Tree', 'Surrealism');
INSERT INTO [dbo].[Classification](ArtWork,Genre)VALUES('Mona Lisa', 'Portrait');
INSERT INTO [dbo].[Classification](ArtWork,Genre)VALUES('Mona Lisa', 'Renaissance');
INSERT INTO [dbo].[Classification](ArtWork,Genre)VALUES('The Vitruvian Man', 'Renaissance');
INSERT INTO [dbo].[Classification](ArtWork,Genre)VALUES('Ebru', 'Tesselation');
INSERT INTO [dbo].[Classification](ArtWork,Genre)VALUES('Honey Is Sweeter Than Blood', 'Surrealism');
