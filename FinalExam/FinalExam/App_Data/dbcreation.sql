CREATE DATABASE [ArtWorksDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ArtWorksDB', FILENAME = N'C:\Users\Christopher\Documents\School\CS460\AssignmentOne\cs460_easton\FinalExam\FinalExam\App_Data\ArtWorksDB.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ArtWorksDB_log', FILENAME = N'C:\Users\Christopher\Documents\School\CS460\AssignmentOne\cs460_easton\FinalExam\FinalExam\App_Data\ArtWorksDB_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
GO