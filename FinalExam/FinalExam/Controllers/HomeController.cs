﻿using FinalExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalExam.Controllers
{
    public class HomeController : Controller
    {
        private DBContext db = new DBContext();

        // GET: Home
        /// <summary>
        /// Welcome Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var Genrelist = db.Genres.ToList();
            ViewBag.GenreCount = Genrelist.Count;
            return View(Genrelist);
        }

        

        public JsonResult GenreList(string i)
        {
            
            var genres = db.Classifications.Where(p => p. Genre==i).Select(a => a.ArtWork).ToArray();
            int c = genres.Length;
            string webpass = "<td>";
            for (int mynum = 0; mynum< c; mynum++)
            {

                webpass += genres[mynum];
            }
            webpass += "</td>";
            var data =
                new
                { name = webpass};
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Artists
        /// <summary>
        /// List of artits sorted by name
        /// </summary>
        /// <returns></returns>
        public ActionResult Artists()
        {
            var Art = db.Artists.OrderBy(a => a.Name).ToList();
            return View(Art);
        }

        // GET: ArtWork
        /// <summary>
        /// List of ArtWorks sorted by Title
        /// </summary>
        /// <returns></returns>
        public ActionResult ArtWork()
        {
            var Art = db.ArtWorks.OrderBy(a => a.Title).ToList();
            return View(Art);
        }

        // GET: Classifiacation
        /// <summary>
        /// List of Classifications sorted by Genre
        /// </summary>
        /// <returns></returns>
        public ActionResult Classification()
        {
            var Clas = db.Classifications.OrderBy(a => a.Genre).ToList();
            return View(Clas);
        }


    }
}