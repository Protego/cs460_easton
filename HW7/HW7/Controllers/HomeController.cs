﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace HW7.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }



        public JsonResult ticker(int? h, int? w, string id)
        {
            if (id == null)
            {
                id = "CPA";
            }
            var link = $"http://chart.finance.yahoo.com/table.csv?s={id}&a=10&b=5&c=2016&d=11&e=5&f=2016&g=d&ignore=.csv";

            // Create a request for the URL. 		
            WebRequest request = WebRequest.Create(link);
            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Display the status.
            Console.WriteLine(response.StatusDescription);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Display the content.
            reader.Close();
            dataStream.Close();
            response.Close();

            if (h == null) { h = 300; }
            if (w == null) { w = 400; }

            var data = new
            {
                Pagelink = responseFromServer,
                height = h,
                width = w,
                ticker = id
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }


    }
}