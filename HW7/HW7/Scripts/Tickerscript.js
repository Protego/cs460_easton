﻿// do an ajax call to get some numbers
$("#request").click(function () {
    var a = $("#tic").val();
    console.log(a);
    var height = $("#height").val();
    var width = $("#wid").val();
    var source = "/Home/ticker/";
    console.log(source);
    // get data in JSON format from our controller
    $.ajax({
        type: "GET",
        dataType: "json",
        url: source,
        data: {h:height,w:width,id:a },
        success: displayData
    });
});

function displayData(data) {
    

    g = new Dygraph(
    document.getElementById("graph"),
    data.Pagelink, {
        
        width: data.w,
        height:data.h,
        ylabel: 'Open, Close, Hi, Low',
        y2label: 'Volume',
        series: {
            'Volume': {
                axis: 'y2'
            }
        },
        axes: {
            y: {
                // set axis-related properties here
                drawGrid: false,
                independentTicks: false
            },
            y2: {
                // set axis-related properties here
                labelsKMB: true,
                drawGrid: true,
                independentTicks: true
            }
        }
    }
  );



    $("#TickerID").text("The ticker you requested: " + data.ticker);
    console.log("In func");
}