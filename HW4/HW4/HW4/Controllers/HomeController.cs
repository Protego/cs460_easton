﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW4.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        /// <summary>
        /// Returns the base index page view.
        /// </summary>
        /// <returns>Returns the view</returns>
        public ActionResult Index()
        {
            return View();
        }

        //GET: Problem1
        /// <summary>
        /// Takes in data from query string and adds it to ViewBag.Data for view access.
        /// </summary>
        /// <returns>Returns the view</returns>
        public ActionResult Problem1()
        {
            string[] data = { Request.QueryString["crn"], Request.QueryString["course"], Request.QueryString["day"], Request.QueryString["time"] };
            ViewBag.Data = data;
            return View();
        }

        //GET: Problem2
        /// <summary>
        /// Gets the base view for Problem2 without passing any data.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Problem2()
        {
            
            return View();
        }
        
        //POST: Problem2
        /// <summary>
        /// Collects the data in a form and assigns each item to a variable then sets a ViewBag.Message before returning the view.
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        public ActionResult Problem2(FormCollection form)
        {
            string crn = form["crn"];
            string name = form["course"];
            string day = form["day"];
            string time = form["time"];
            ViewBag.Message = "Course Information Submitted.";
            return View();
        }


        //GET: Problem3
        /// <summary>
        /// Adds a ViewBag.Message and returns the view.
        /// </summary>
        [HttpGet]
        public ActionResult Problem3()
        {
            ViewBag.Message = "This will calculate a 10 year loan. Enter your initial loan ammount and the interest rate.";
            return View();
        }

        //POST: Problem3
        /// <summary>
        /// Takes in two input items, prin and rate and calculates the total value after 10 years assuming no payments were made.
        /// This is added to the ViewBag.Message and the view is returned.
        /// </summary>
        /// <param name="prin"></param>
        /// <param name="rate"></param>
        [HttpPost]
        public ActionResult Problem3(double prin, double rate)
        {
            double total = prin;
            for (int i = 0; i < 10; i++)
            {
                total = total + (total * rate);
            }
            total = Math.Round(total, 2);
            string msg = "Assuming you make no payments because you are bad with money, your total will be: " +total;
            ViewBag.Message = msg;
            return View();
        }
    }
}