namespace HW5v2.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Requests : DbContext
    {
        public Requests()
            : base("name=Requests")
        {
        }

        public virtual DbSet<ChangeRequest> ChangeRequests { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChangeRequest>()
                .Property(e => e.MiddleI)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
