﻿using HW5.DAL;
using HW5.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW5.Controllers
{
    public class HomeController : Controller
    {
        private RequestContext db = new RequestContext();

        public ActionResult Index()
        {
            return View();
        }

        // GET: List all the users
        public ActionResult CurrentRequest()
        {
            //return View(users.theUsers);
            return View(db.Request.ToList());
        }

        // GET: Page with a form to create a new user and add them
        // to the system
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Request request)
        {
            if (ModelState.IsValid)
            {
                Debug.WriteLine("User in Create [POST]: " + request);
                //users.theUsers.Add(user);
                db.Request.Add(request);
                db.SaveChanges();
                //Debug.WriteLine(users.theUsers);
                // use the object
                return RedirectToAction("Index");   // return a HTTP 302
            }
            return View(request);
        }
    }
}