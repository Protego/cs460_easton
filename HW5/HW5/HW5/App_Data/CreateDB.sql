﻿CREATE DATABASE [PirateDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PirateDB', FILENAME = N'C:\Users\Christopher\Documents\School\CS460\AssignmentOne\cs460_easton\HW5\HW5\HW5\App_Data\cs460.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PirateDB_log', FILENAME = N'C:\Users\Christopher\Documents\School\CS460\AssignmentOne\cs460_easton\HW5\HW5\HW5\App_Data\cs460_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
GO