﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HW5.Models;

namespace HW5.DAL
{
    public class RequestContext : DbContext
    {
        public DbSet<Request> Request { get; set; }
    }
}