﻿using HW5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HW5.DAL
{
    public class RequestCollection
    {
        public List<Request> MyRequests;

        public RequestCollection()
        {
            MyRequests = new List<Request>
            {
                new Request
                {
                    FirstName="Jim", LastName="Bob",MiddleI="D",StudentID="9990099",Phone="555-555-5555",Email="JimB@mail.com",ChangeRequest="I want to drop all classes."
                }
            };

        }
    }
}