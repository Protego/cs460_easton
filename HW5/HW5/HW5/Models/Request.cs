namespace HW5.Models
{
    using System;
    using System.Data.Entity;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    public class Request
    {
        public int ID { get; set; }

        [Display(Name = "First Name"), Required]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Middle Initial")]
        public string MiddleI { get; set; }

        [Required, DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Phone ###-###-####")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "Student ID")]
        public string StudentID { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Request Details")]
        public string ChangeRequest { get; set; }
    }
}