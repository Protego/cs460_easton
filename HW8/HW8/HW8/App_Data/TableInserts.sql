-- ########### Pirates ###########
INSERT INTO [dbo].[Pirates](Name,Date)VALUES('Mitch','1645-11-21');
INSERT INTO [dbo].[Pirates](Name,Date)VALUES('BugEye','1647-10-01');
INSERT INTO [dbo].[Pirates](Name,Date)VALUES('Frank','1638-11-18');
INSERT INTO [dbo].[Pirates](Name,Date)VALUES('Holla','1639-9-11');
INSERT INTO [dbo].[Pirates](Name,Date)VALUES('Shammy','1639-7-28');

-- ########### Ship ###########

INSERT INTO [dbo].[Ships](Name,Type,Tonnage)VALUES('Butterfly','Frigate',300);
INSERT INTO [dbo].[Ships](Name,Type,Tonnage)VALUES('Scarecrow','Frigate',250);
INSERT INTO [dbo].[Ships](Name,Type,Tonnage)VALUES('Nightmare','PaddleBoat',1);
INSERT INTO [dbo].[Ships](Name,Type,Tonnage)VALUES('BlueShadow','Caravel',130);
INSERT INTO [dbo].[Ships](Name,Type,Tonnage)VALUES('CalmWaters','Caravel',150);


-- ########### Crew ###########

INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(1,1,18.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(2,1,18.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(3,1,18.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(4,1,18.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(5,1,18.00);

INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(1,2,18.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(2,2,18.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(3,2,18.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(4,2,18.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(5,2,18.00);

INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(1,3,30.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(2,3,30.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(3,3,30.00);

INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(4,3,40.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(5,3,40.00);

INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(1,4,20.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(2,4,20.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(3,4,20.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(4,4,20.00);

INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(5,5,20.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(2,5,20.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(3,5,20.00);
INSERT INTO [dbo].[Crews](PID,SID,BootyValue)VALUES(4,5,20.00);
