IF OBJECT_ID('dbo.Crews','U') IS NOT NULL
	DROP TABLE [dbo].[Crews];
GO

IF OBJECT_ID('dbo.Pirates','U') IS NOT NULL
	DROP TABLE [dbo].[Pirates];
GO

IF OBJECT_ID('dbo.Ships','U') IS NOT NULL
	DROP TABLE [dbo].[Ships];
GO
-- ########### Pirates ###########
CREATE TABLE [dbo].[Pirates]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (50) NOT NULL,
	[Date] Date NOT NULL,
	CONSTRAINT [PK_dbo.Pirates] PRIMARY KEY CLUSTERED ([ID] ASC)
);

-- ########### Ship ###########
CREATE TABLE [dbo].[Ships]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (50) NOT NULL,
	[Type] NVARCHAR (100) NOT NULL,
	[Tonnage] INT,
	CONSTRAINT [PK_dbo.Ships] PRIMARY KEY CLUSTERED ([ID] ASC)
);

-- ########### Crew ###########
CREATE TABLE [dbo].[Crews]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[PID] INT NOT NULL,
	[SID] INT NOT NULL,
	[BootyValue] DEC,
	CONSTRAINT [PK_dbo.Crews] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [FK_dbo.Crews_dbo.Ships_ID] FOREIGN KEY ([SID]) REFERENCES [dbo].[Ships] ([ID]),
	CONSTRAINT [FK_dbo.Crews_dbo.Pirates_ID] FOREIGN KEY ([PID]) REFERENCES [dbo].[Pirates] ([ID])
);
