CREATE DATABASE [PirateDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PirateDB', FILENAME = N'C:\Users\Christopher\Documents\School\CS460\AssignmentOne\cs460_easton\HW8\HW8\HW8\App_Data\PirateDB.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PirateDB_log', FILENAME = N'C:\Users\Christopher\Documents\School\CS460\AssignmentOne\cs460_easton\HW8\HW8\HW8\App_Data\PirateDB_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
GO