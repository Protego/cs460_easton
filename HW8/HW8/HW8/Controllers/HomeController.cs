﻿using HW8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW8.Controllers
{
    public class HomeController : Controller
    {
        private PirateContext db = new PirateContext();
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // GET: Home
        public ActionResult Booty(int? page)
        {
            if (page == null) { page = 1; }
            int pirateperpage = 3;
            var pirates = db.Pirates.ToList();

            var x = db.Crews.GroupBy(c => c.PID).Select(p => new {
                            Guy = p.Key,
                            TotBoot = p.Sum(b =>b.BootyValue)}).OrderByDescending(tb => tb.TotBoot).ToList();


            double numofpages = Math.Ceiling((double)pirates.Count / pirateperpage);
            ViewBag.numofpages = numofpages;

            var piratepass = pirates.Skip(pirateperpage * ((int)page - 1)).Take(pirateperpage);

            return View(piratepass);
        }
        /// <summary>
        /// Gets the booty value, name, and number of crews a given pirate ID.
        /// </summary>
        /// <param name="id"></param>
        public JsonResult BootyDetails(int? id)
        {
            if (id == null)
            {
                id = 1;
            }
            var pirateobj = db.Pirates.Where(x => x.ID == id).First();
            var crewlist = db.Crews.Where(p => p.PID == id).ToList();
            int crewcount = crewlist.Count;
            double bootyvalue = 0;
            foreach (Crew c in crewlist)
            {
                bootyvalue = bootyvalue + ((double)c.BootyValue * (double)(db.Ships.Where(x => x.ID == c.SID).First().Tonnage));
            }

            var data = new
            {
                Message = "Here is the booty information.",
                pirate = pirateobj.Name,
                list = crewcount,
                booty = bootyvalue
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Ships
        public ActionResult Ships()
        {
            var ships = db.Ships.OrderByDescending(p => p.Name).ToList();

            return View(ships);
        }

        // GET: Home
        public ActionResult Crews()
        {
            var crews = db.Crews.OrderBy(p => p.SID).ToList();

            return View(crews);
        }


    }
}