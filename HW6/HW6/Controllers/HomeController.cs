﻿using HW6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HW6.Views
{
    public class HomeController : Controller
    {
        private AdvWorksContext db = new AdvWorksContext();
        // GET: Home
        /// <summary>
        /// Displays the Main categories sorted descending by name.
        /// </summary>
        public ActionResult Index()
        {
            var products = db.ProductCategories.OrderByDescending(p => p.Name).ToList();
            return View(products);
        }

        /// <summary>
        /// Displays the subcategories based off the ID of the parent category.
        /// </summary>
        /// <param name="ID">Category ID</param>
        public ActionResult CategoryResults(int? ID)
        {
            if (ID == null) { ID = 1; }
            var products = db.ProductSubcategories.Where(n => n.ProductCategoryID == ID).OrderByDescending(p => p.Name);
            return View(products);
        }

        /// <summary>
        /// Lists of products in the subcategory
        /// </summary>
        /// <param name="ID">Subcategory ID</param>
        public ActionResult ProductList(int? ID)
        {
            if (ID == null) { ID = 1; }
            var products = db.Products.Where(n => n.ProductSubcategoryID == ID).OrderByDescending(p => p.Name);
            return View(products);
        }

        /// <summary>
        /// Displays the details of a product.
        /// </summary>
        /// <param name="ID">Product ID</param>
        public ActionResult ProductDetails(string ID)
        {
            var product = db.Products.Where(n => n.ProductNumber == ID).First();
            return View(product);
        }
        

        //Get
        /// <summary>
        /// Returns a specific product review.
        /// </summary>
        /// <param name="ID">Review ID</param>
        public ActionResult Reviews(int ID)
        {
            if (ID == null) { ID = 709; }
            var product = db.Products.Where(n => n.ProductID == ID).First();
            return View(product.ProductReviews);
        }

        //Not Used at this time...
        //Get
        [HttpGet]
        public ActionResult NewReview(int? ID)
        {

            if (ID == null) { ID = 1; }
            ViewBag.id = (int)ID;
            return View();
        }

        //Not used at this time
        //Post
        [HttpPost]
        public ActionResult NewReview([Bind(Include = "ProductID,ReviewerName,ReviewDate,EmailAddress,Rating,Comments,ModefiedDate")] ProductReview review)
        {
            if (ModelState.IsValid)
            {
                db.ProductReviews.Add(review);
                db.SaveChanges();
                return RedirectToAction("Reviews/"+ review.ProductID);
            }

            return View(review.ProductID);
        }
    }
}