﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
    class Node
    {
        /// <summary>
        ///  Empty constructor for Node
        /// </summary>
        public Node()
        {
            data = null;
            next = null;
        }

        /// <summary>
        /// Constructor for Node class that takes both a data and link to next node.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="next"></param>
        public Node (Object data, Node next)
        {
            this.data = data;
            this.next = next;
        }


        public Object data { get; set; }
        public Node next { get; set; }
    }
}
