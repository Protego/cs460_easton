﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
    class Calculator
    {

        private StackADT stack = new LinkedStack();

        /// <summary>
        /// Entry point method
        /// </summary>
        /// <param name="args">Command line arguments</param>
        static void Main(string[] args)
        {
            Calculator app = new Calculator();
            bool playAgain = true;
            Console.WriteLine("\nCalc");
            while (playAgain)
            {
                playAgain = app.doCalc();
            }
            Console.WriteLine("Bye.");
        }

        /// <summary>
        /// Takes in user input and performs calculation. 
        /// </summary>
        /// <returns>true if calculation succeeds, false if user wants to quit</returns>
        private bool doCalc()
        {
            Console.WriteLine("Please enter q to quit\n");
            string input = "2 2 +";
            Console.WriteLine("Format = "+input+"\n");

            Console.WriteLine("> ");

            input = Console.ReadLine();

            if (input.StartsWith("q") || input.StartsWith("Q"))
            {
                return false;
            }

            string output = "4";
            try
            {
                output = evaluateInput(input);
            }catch(ArgumentException e)
            {
                output = e.ToString();
            }
            Console.WriteLine("\n\t>>>"+input+" = "+output);
            return true;
        }

        /// <summary>
        /// Evaluates user input string
        /// </summary>
        /// <param name="input">User math input as a string</param>
        /// <returns>The answer as a string</returns>
        private string evaluateInput(string input)
        {
            if (input == null || input.Equals(""))
            {
                throw new ArgumentException("Invalid Input.");
                
            }

            stack.Clear();

            string[] parsed = input.Split();
            string s = "+";
            double a=0;
            double b=0;
            double c = 0;

            foreach(var val in parsed)
            {
                double num;
                if (Double.TryParse(val, out num))
                    stack.Push(num);
                else
                {
                    s = val;
                    if (s.Length > 1)
                        throw new ArgumentException("Invalid Operand");

                    if (stack.IsEmpty())
                        throw new ArgumentException("Bad Format");
                    b = (double)stack.Pop();
                    if (stack.IsEmpty())
                        throw new ArgumentException("Bad Format");
                    a = (double)stack.Pop();
                    c = doOperation(a, b, s);
                }
            }
            return c.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public double doOperation(double a, double b, string s)
        {
            double c = 0.0;
            if (s.Equals("+")) {c = (a + b); }
            else if (s.Equals("-")) { c = (a - b); }
            else if (s.Equals("*")) { c = (a * b); }
            else if (s.Equals("/"))
            {
                try
                {
                    c = (a / b);
                    if (Double.IsNegativeInfinity(c)|| Double.IsPositiveInfinity(c))
                    {
                        throw new ArgumentException("Cant divide by zero.");
                    }
                }
                catch (ArgumentException e)
                {
                    throw new ArgumentException(e.ToString());
                }
            }
            else { throw new ArgumentException("Bad operand"); }

            return c;

        }
        
    }
}
