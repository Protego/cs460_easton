﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW3
{
    class LinkedStack : StackADT
    {
        private Node top;

        /// <summary>
        /// Constructor, creates empty stack
        /// </summary>
        public LinkedStack()
        {
            top = null;
        }

        /// <summary>
        /// Empties the stack
        /// </summary>
        public void Clear()
        {
            top = null;    
        }

        /// <summary>
        /// checks if the stack is empty
        /// </summary>
        /// <returns>Returns true if empty</returns>
        public bool IsEmpty()
        {
            return top == null;
        }

        /// <summary>
        /// Gets the top object on the stack.
        /// </summary>
        /// <returns>Returns the first/top object that is on the stack or null if empty.</returns>
        public object Peek()
        {
            if (IsEmpty())
            {
                return null;
            }
            return top.data;
        }

        /// <summary>
        /// Removes the top object from the stack
        /// </summary>
        /// <returns>Returns the object that is removed from the top of the stack.</returns>
        public object Pop()
        {
            if (IsEmpty())
            {
                return null;
            }
            Object topItem = top.data;
            top = top.next;
            return topItem;
        }

        /// <summary>
        /// Adds a new item to the stock.
        /// </summary>
        /// <param name="newItem"></param>
        /// <returns>Returns the new item that was added to the stack.</returns>
        public object Push(object newItem)
        {
            if(newItem == null)
            {
                return null;
            }
            Node newNode = new Node(newItem, top);
            top = newNode;
            return newItem;
        }
    }
}
